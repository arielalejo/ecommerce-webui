import React, { useState, useEffect }  from 'react';
import ProductList from './components/ProductList';
import NavBar from './components/NavBar';
import Cart from './components/Cart';
import Checkout from './components/Checkout';
import {Switch, Route, Redirect} from 'react-router-dom';
import {getProducts} from './services/productService';
import * as cartService from './services/cartService';
import './App.css';

export default function App() {    
    const [products, setProducts] =  useState([]);
    const [cart, setCart] = useState({});

    const fetchProducts = async () => {
        const {data} = await getProducts();
        
        setProducts(data);
    };

    const fetchCart = async () => {
        setCart(await cartService.getCart());
    };

    useEffect(() => {
        fetchProducts();
        fetchCart();
    }, []);

    const handleAddProductToCart = async (productId, quantity ) => {
        const item = await cartService.addProductToCart(productId, quantity);
        setCart(item.cart);
    };

    const handleUpdateProductQty = async (productId, newQuantity) => {
        const {cart} = await cartService.updateProductQuantity(productId, newQuantity);
        setCart(cart);
    };

    const handleRemoveProductFromCart = async (productId) => {
        const {cart} = await cartService.removeProduct(productId);
        setCart(cart);
    };

    const handleEmptyCart = async () => {
        const {cart} = await cartService.emptyCart();
        setCart(cart);
    };

    const refreshCart = async() => {
        const newCart = await cartService.refreshCart();
        setCart(newCart);
    };  

    return (
        <>
            <NavBar totalItems={cart.total_items}/>
            <Switch>
                <Route 
                    path="/products" 
                    render={ props => <ProductList {...props} products={products} onAddProductToCart={handleAddProductToCart}/> }
                />
                <Route
                    path="/cart"
                    render={props => <Cart {...props} cart={cart} onEmptyCart={handleEmptyCart} onUpdateProductQty={handleUpdateProductQty} onRemoveProductFromCart={handleRemoveProductFromCart}/>  }
                />
                <Route
                    path="/checkout" 
                    render={props => (cart.id && 
                        <Checkout 
                            {...props} 
                            cart={cart} 
                            refreshCart={refreshCart}
                        />)}
                />
                <Redirect from="/" exact to="/products" />                
            </Switch>
        </>
    );
}
