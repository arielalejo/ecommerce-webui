import { loadStripe } from '@stripe/stripe-js';

export const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_API_KEY);

export const createPaymentMethod = (stripeObject, cardElement) => {
    return stripeObject.createPaymentMethod({type:'card', card: cardElement});
};