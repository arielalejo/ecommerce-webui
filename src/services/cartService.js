import {commerce} from './commerce';

export const getCart = () => {
    return commerce.cart.retrieve();
};

export const addProductToCart = (productId, quantity)  => {
    return commerce.cart.add(productId, quantity);
};

export const updateProductQuantity = (productId, quantity) => {
    return commerce.cart.update(productId, {quantity});
};

export const removeProduct = (productId) => {
    return commerce.cart.remove(productId);
};

export const emptyCart = () => {
    return commerce.cart.empty();
};

export const refreshCart = () => {
    return commerce.cart.refresh();
};