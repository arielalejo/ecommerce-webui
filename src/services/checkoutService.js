import { commerce } from './commerce';

export function getCheckoutToken(cartId) {
    return commerce.checkout.generateTokenFrom('cart', cartId);
}

export function  getShippingCountries (checkoutTokenId) {
    return commerce.services.localeListShippingCountries(checkoutTokenId);
}

export function getShippingSubdivitions(countryCode) {
    return commerce.services.localeListSubdivisions(countryCode);
}

export function getShippingOptions(checkoutTokenId, country, region = null){
    return commerce.checkout.getShippingOptions(checkoutTokenId, {country, region});
}


export function captureCheckout(checkoutTokenId, order){
    return commerce.checkout.capture(checkoutTokenId, order);
}