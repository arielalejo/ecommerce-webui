import {commerce} from './commerce';

export const getProducts = () => {
    return commerce.products.list();
};