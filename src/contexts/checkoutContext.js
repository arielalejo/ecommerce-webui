import { createContext } from 'react';

export const CheckoutContext = createContext();
CheckoutContext.displayName = 'CheckoutContext';
