import { Card, CardActions, CardMedia, CardContent, Typography, IconButton } from '@material-ui/core';
import { AddShoppingCart } from '@material-ui/icons';
import React from 'react';
import { useStyles } from './styles';

export default function Product({product, onAddProductToCart}) {
    const classes = useStyles();

    return (
        <>
            <Card className={classes.root}>
                <CardMedia className={classes.media} image={product.media.source} title={product.name}/>
                <CardContent>
                    <div className={classes.cardContent}>
                        <Typography variant="h5" gutterBottom>
                            {product.name}
                        </Typography>
                        <Typography variant="h5" gutterBottom>
                            {product.price.formatted_with_code}
                        </Typography>

                    </div>
                    <Typography variant="body2" color="textSecondary" dangerouslySetInnerHTML={{__html: product.description}}/>                      
                </CardContent>
                <CardActions className={classes.cardActions}>
                    <IconButton aria-label="Add to cart" onClick={()=>onAddProductToCart(product.id, 1)}>
                        <AddShoppingCart/>
                    </IconButton>
                </CardActions>
            </Card>
        </>
    );
}
