import React, {useState, useEffect} from 'react';
import useStyles from './styles';
import Stepper from '@material-ui/core/Stepper';
import Typography from '@material-ui/core/Typography';
import { CssBaseline, Paper, Step, StepLabel } from '@material-ui/core';
import CheckoutForm from '../CheckoutForm';
import { getCheckoutToken } from '../../services/checkoutService';
import { useHistory } from 'react-router-dom';
import { CheckoutContext } from '../../contexts/checkoutContext';

const steps = ['Address Information', 'Payment Details'];

export default function Checkout({cart, refreshCart}) {
    const history = useHistory();
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const [checkoutToken, setCheckoutToken] = useState(null);
    const [checkoutFormData, setCheckoutFormData] = useState({});
    const [order, setOrder] = useState({});
    const [errorMessage, setErrorMessage] = useState('');
    const [isFinished, setIsFinished] = useState(false);
    
    const getToken = async() => {
        try{
            const token = await getCheckoutToken(cart.id);
            setCheckoutToken(token);
        } catch(e){
            if (activeStep !== steps.length) history.push('/');
        }
    };

    useEffect(() => {
        getToken();
    }, []);

    const nextStep = () => {setActiveStep((prevStep) => prevStep + 1);};
    
    const previusStep = () => {setActiveStep((prevStep) => prevStep - 1);};

    const next = (data) => {
        setCheckoutFormData(data);
        nextStep();
    };

    const timetOut = () => {
        setTimeout(() => {
            setIsFinished(true);
        }, 3000);
    };

    const handleCaptureCheckout = async (checkoutTokenId, newOrder) =>{
        try {
            // const incomingOrder = await checkoutService.captureCheckout(checkoutTokenId, newOrder);
            // setOrder(incomingOrder);
            refreshCart();
            nextStep();
            timetOut();
        } catch (error) {
            setErrorMessage(error.data.error.message);
        }
    };

    const contextValue = {
        step: activeStep,
        checkoutToken: checkoutToken,
        next: next,
        checkoutFormData: checkoutFormData,
        prevStep: previusStep,
        onCaptureCheckout: handleCaptureCheckout,
        order: order,
        error: errorMessage,
        isFinished: isFinished
    };
    
    return (
        <>
            <CssBaseline/>
            <div className={classes.toolbar}/>
            <CheckoutContext.Provider value={contextValue}>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Typography variant="h4" color="initial" align="center">Checkout</Typography>
                        <Stepper activeStep={activeStep}>
                            {steps.map(step=>(
                                <Step key={step}>
                                    <StepLabel>{step}</StepLabel>
                                </Step>
                            ))}
                        </Stepper>
                        { checkoutToken && <CheckoutForm /> }
                    </Paper>
                </main>
            </CheckoutContext.Provider>
        </>
    );
}
