import React from 'react';
import { Card, CardActions, CardMedia, CardContent, Typography, IconButton, Button } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import { useStyles } from './styles';

export default function CartItem({product, onUpdateProductQty, onRemoveProductFromCart}) {
    const classes = useStyles();

    return (
        <div>
            <Card>
                <CardMedia
                    className={classes.media}
                    title={product.name}
                    image={product.media.source}
                />
                <CardContent>
                    <Typography variant="h5" color="initial">{product.name}</Typography>
                    <Typography variant="h5" color="initial">{product.price.formatted_with_code}</Typography>
                </CardContent>
                <CardActions>
                    <IconButton aria-label="decrement quantity" onClick={()=>onUpdateProductQty(product.id, product.quantity - 1)}> 
                        <RemoveCircleIcon/>
                    </IconButton>
                    <Typography variant="subtitle1" color="initial">{product.quantity}</Typography>
                    <IconButton aria-label="increment quantity" onClick={()=>onUpdateProductQty(product.id, product.quantity + 1)}>
                        <AddCircleIcon/>
                    </IconButton>

                    <Button variant="contained" color="secondary" onClick={()=>onRemoveProductFromCart(product.id)}>
                        Remove
                    </Button>
                </CardActions>
            </Card>
        </div>
    );
}
