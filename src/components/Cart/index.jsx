import React from 'react';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CartItem from '../CartItem';
import {useStyles} from './styles';

function Cart({cart, onEmptyCart, onUpdateProductQty, onRemoveProductFromCart}) {
    const classes = useStyles();
    
    const EmptyCart = () => (
        <Typography variant="subtitle1" color="initial">
            yo dont have items yet <Link to="/products"> start adding!</Link>
        </Typography>
    );

    const FilledCart = () => (
        <>            
            <Grid container spacing={3}>
                {cart.line_items.map(product => (
                    <Grid item key={product.id} xs={12} sm={4}>
                        <CartItem product={product} onUpdateProductQty={onUpdateProductQty} onRemoveProductFromCart={onRemoveProductFromCart}/>
                    </Grid>
                ))}
            </Grid> 
            <div className={classes.cardDetails}>
                <Typography variant="h4" color="initial">Subtotal: {cart.subtotal.formatted_with_code}</Typography>
                <div>
                    <Button variant="contained" color="secondary" className={classes.emptyButton} onClick={onEmptyCart}>
                        Clear Cart
                    </Button>
                    <Button variant="contained" color="primary" className={classes.checkoutButton} component={Link} to="/checkout">
                        Checkout
                    </Button>    
                </div>
                
            </div>            
        </>
    );

    if (!cart.line_items) {
        return 'Loading...';
    }
    
    return (
        <Container >
            <div className={classes.toolbar}/>
            <Typography variant="h3" className={classes.title} style={{padding: '20px 0'}}>
                Your cart 
            </Typography> 
            {!cart.line_items.length ? <EmptyCart/> : <FilledCart/>}
            
        </Container>
    );
}

export default Cart;
