import React from 'react';
import {Link, useLocation} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { ShoppingCart } from '@material-ui/icons';
import { Badge } from '@material-ui/core';
import {useStyles} from './styles';
import logo from '../../assets/ecommerce-logo.jpeg';

export default function NavBar({totalItems}) {
    const classes = useStyles();
    const location = useLocation();

    return (
        <div className={classes.appBar}>
            <AppBar position="fixed" color="primary">
                <Toolbar>
                    <IconButton aria-label="menu" edge="start" component={Link} to="/products">
                        <img src={logo} alt="mycommerce" height="55px" className={classes.image}/>
                    </IconButton>
                    <Typography variant="h5" className={classes.title}>
                        My E-commerce                        
                    </Typography>
                    {location.pathname === '/products' && (
                        <Link to="/cart">
                            <IconButton aria-label="show cart items">
                                <Badge badgeContent={totalItems} color="secondary" >
                                    <ShoppingCart className={classes.cartIcon}/>
                                </Badge>
                            </IconButton>                    
                        </Link>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
}
