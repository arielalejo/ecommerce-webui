import React, {useContext} from 'react';
import * as stripeService from '../../../services/stripe';
import { Typography, Button, Divider} from '@material-ui/core';
import { Elements, CardElement, ElementsConsumer} from '@stripe/react-stripe-js';
import Review from '../Review/index';
import { CheckoutContext } from '../../../contexts/checkoutContext';

export default function PaymentDetails() {
    const checkContxt = useContext(CheckoutContext);
    const handleSubmit = async (event, elements, stripeObject) => {
        event.preventDefault();

        if (!stripeObject || !elements) return;

        // console.log('elements', elements);
        // console.log('stripe', stripe);

        const cardElement = elements.getElement(CardElement);

        const {error, paymentMethod } = await stripeService.createPaymentMethod(stripeObject, cardElement);

        if (error){
            console.log('error', error);
        } else {
            //console.log('paymentMeth', paymentMethod);
            const orderData = {
                line_items: checkContxt.checkoutToken.live.line_items,
                customer: {
                    firstname: checkContxt.checkoutFormData.firstName,
                    lastname: checkContxt.checkoutFormData.lastName,                    
                    email: checkContxt.checkoutFormData.email,                    
                },
                shipping: {
                    name: 'Primary',
                    street: checkContxt.checkoutFormData.address1,
                    country: checkContxt.checkoutFormData.shippingCountry,
                    country_state: checkContxt.checkoutFormData.shippingSubdivision,
                    postal_zip: checkContxt.checkoutFormData.zip
                },
                fulfillment: { shipping_method: checkContxt.checkoutFormData.shippingOption},
                payment: {
                    gateway: 'stripe',
                    stripe: {
                        payment_method_id: paymentMethod.id
                    }
                }
            };

            //console.log('orderData:', orderData)
            checkContxt.onCaptureCheckout(checkContxt.checkoutToken.id, orderData);
        }

    };

    return ( 
        <>
            <Review />
            <Divider/>
            <Typography variant="h6" gutterBottom style={{margin:'20px 0'}}> Payment Method</Typography>
            <Elements stripe={stripeService.stripePromise}>
                <ElementsConsumer>
                    {({elements, stripe})=>(
                        <form onSubmit={e => handleSubmit(e, elements, stripe)}>
                            <CardElement/>
                            <div style={{marginTop:'30px', display:'flex', justifyContent:'space-between'}}>
                                <Button variant="outlined" onClick={checkContxt.previusStep}>Back</Button>
                                <Button variant="contained" color="primary" type="submit" disabled={!stripe}>
                                    Pay {checkContxt.checkoutToken.live.subtotal.formatted_with_code}
                                </Button>
                            </div>
                        </form>
                    )

                    }
                </ElementsConsumer>
            </Elements>
        </>
    );
}
