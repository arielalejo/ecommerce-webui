import React, { useState, useEffect, useContext } from 'react';
import { useForm, FormProvider } from 'react-hook-form';
import { getShippingCountries, getShippingSubdivitions, getShippingOptions } from '../../../services/checkoutService';
import {
    Grid,
    Typography,
    NativeSelect,    
    InputLabel, Button
} from '@material-ui/core';
import InputForm from '../InputForm';
import {Link} from 'react-router-dom';
import { CheckoutContext } from '../../../contexts/checkoutContext';


export default function AddressForm() {
    const checkContxt = useContext(CheckoutContext);
    const methods = useForm();
    const [shippingCountries, setShippingCountries] = useState([]);
    const [shippingCountry, setShippingCountry] = useState('');
    const [shippingSubdivisions, setShippingSubdivisions] = useState([]);
    const [shippingSubdivision, setShippingSubdivision] = useState('');
    const [shippingOptions, setShippingOptions] = useState([]);
    const [shippingOption, setShippingOption] = useState('');

    const fetchShippingCountries = async () => {
        try {
            const response = await getShippingCountries(checkContxt.checkoutToken.id);

            let countries = Object.entries(
                response.countries
            ).map(([code, name]) => ({ id: code, label: name }));
            
            setShippingCountries(countries);            
            setShippingCountry(countries[0].id);            
        } catch (e) {
            console.log(e);
        }
    };

    const fetchShippingSubdivitions = async () => {
        
        try {
            const response = await getShippingSubdivitions(shippingCountry);            
            const subdivisions = Object.entries(response.subdivisions).map(([code, name]) => ({id: code, label:name}));
            
            setShippingSubdivisions(subdivisions);
            setShippingSubdivision(subdivisions[0].id);
        } catch (e) { 
            console.log(e);
        }

        // **display a <Select />  component which options will be the fetched subdivitions 
    };

    const fetchShippingOption = async () => {
        const response = await getShippingOptions(checkContxt.checkoutToken.id, shippingCountry, shippingSubdivision);
        
        const options = response.map(op => ({id: op.id, label: `${op.description} (charge : ${op.price.formatted_with_code })` }));
        setShippingOptions(options);
        setShippingOption(options[0].id);
    };

    useEffect(() => {
        fetchShippingCountries();
    }, []);

    useEffect(() => {
        if (shippingCountry) fetchShippingSubdivitions();
    }, [shippingCountry]);

    useEffect(()=>{
        if (shippingSubdivision) fetchShippingOption();
    }, [shippingSubdivision]);

    // **************************************************************************************************

    return (
        <div>
            <Typography variant="h4">Address Form</Typography>
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit(data => checkContxt.next({...data, shippingCountry, shippingSubdivision, shippingOption}))}>
                    <Grid container spacing={3}>
                        <InputForm name="firstName" label="First Name" />
                        <InputForm name="lastName" label="Last Name" />
                        <InputForm name="address1" label="Address" />
                        <InputForm name="email" label="Email" />
                        <InputForm name="zip" label="Zip Code" />
                        <Grid item xs={12} sm={6}>
                            <InputLabel htmlFor="country-menu">
                                Country
                            </InputLabel>
                            <NativeSelect                            
                                value={shippingCountry}
                                onChange={(e)=> setShippingCountry(e.target.value)}
                                inputProps={{ name: 'country', id: 'country-menu' }}
                            >
                                { shippingCountries.map(country => (
                                    <option value={country.id} key={country.id}>
                                        {country.label}
                                    </option>
                                )) }
                            </NativeSelect>                            
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <InputLabel htmlFor="subdivision-menu">
                                Subdivision
                            </InputLabel>
                            <NativeSelect                            
                                value={shippingSubdivision}
                                onChange={(e)=> setShippingSubdivision(e.target.value)}
                                inputProps={{ name: 'subdivision', id: 'subdivision-menu' }}
                            >
                                { shippingSubdivisions.map(subdivision => (
                                    <option value={subdivision.id} key={subdivision.id}>
                                        {subdivision.label}
                                    </option>
                                )) }
                            </NativeSelect>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <InputLabel htmlFor="options-menu">
                                Shipping Options
                            </InputLabel>
                            <NativeSelect                            
                                value={shippingOption}
                                onChange={(e)=> setShippingOption(e.target.value)}
                                inputProps={{ name: 'options', id: 'options-menu' }}
                            >
                                { shippingOptions.map(option => (
                                    <option value={option.id} key={option.id}>
                                        {option.label}
                                    </option>
                                )) }
                            </NativeSelect>
                        </Grid>
                    </Grid>
                    <br/>
                    <div style={{display: 'flex', justifyContent:'space-between', marginTop:'20px'}}>
                        <Button component={Link} to="/cart" variant="contained" color="default">
                            Return
                        </Button>
                        <Button variant="contained" color="primary" type="submit">
                            Next Step
                        </Button>
                    </div>

                </form>
            </FormProvider>
        </div>
    );
}
