import React, { useContext } from 'react';
import AddressForm from './AddressForm';
import PaymentDetails from './PaymentDetails';
import Confirmation from './Confirmation/index';
import { CheckoutContext } from '../../contexts/checkoutContext';

export default function CheckOutForm() {
    const checkContxt = useContext(CheckoutContext);

    switch(checkContxt.step){
    case(0):
        return <AddressForm/>;
    case (1):
        return <PaymentDetails/>;        
    default:
        return <Confirmation/>;
    }
}
