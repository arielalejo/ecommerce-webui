import React, { useContext }from 'react';
import Typography from '@material-ui/core/Typography';
import { Divider, Button, CircularProgress } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { CheckoutContext } from '../../../contexts/checkoutContext';


export default function Confirmation() {
    const checkContxt = useContext(CheckoutContext);

    return (checkContxt.order.customer || checkContxt.isFinished)? (
        <>
            <div>
                <Typography variant="h5" color="initial"> Thanks for your purchase , firstanme, lstname</Typography>
                <Divider/>
                <Typography variant="subtitle2" color="initial"> checkContxt.order Ref:</Typography>
            </div>
            <br/>
            <Button component={Link} to="/" variant="outlined" color="default" type="button">
                Back to home
            </Button>
        </>
    ) : (
        <div style={{display: 'flex', justifyContent:'center', alignItems:'center'}}>
            <CircularProgress/>
        </div>
    );
}
