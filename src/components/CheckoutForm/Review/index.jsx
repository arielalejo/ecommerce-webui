import React, { useContext } from 'react';
import { Typography, List, ListItem, ListItemText } from '@material-ui/core';
import { CheckoutContext } from '../../../contexts/checkoutContext';

export default function Review() {
    const checkContxt = useContext(CheckoutContext);
    return (
        <>
            <Typography variant="h5" color="initial" gutterBottom>Order Summary</Typography>
            <List  component="nav" aria-label="secondary mailbox folders">
                {checkContxt.checkoutToken.live.line_items.map(item=>(
                    <ListItem key={item.name}>
                        <ListItemText primary={item.name} secondary={`Quantity: ${item.quantity}`}/>
                        <Typography style={{fontWeight:'bold'}} variant="body2">{item.line_total.formatted_with_code}</Typography>
                    </ListItem>
                ))}
                <br/>
                <ListItem>
                    <ListItemText primary="Total:" ></ListItemText>
                    <Typography variant="subtitle1" style={{fontWeight: 700}}>{checkContxt.checkoutToken.live.subtotal.formatted_with_code}</Typography>
                </ListItem>                
            </List>            
        </>
    );
}
