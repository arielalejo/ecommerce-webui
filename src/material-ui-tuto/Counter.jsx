import React from 'react';

/* 
    Sample component for testing
*/
export default function Counter() {
    const [gifts, setGifts] = React.useState([]);

    const addGifts = () => {
        const clonedGifts = [...gifts];
        clonedGifts.push({
            id: clonedGifts.length+1
        });
        setGifts(clonedGifts);
    };
    
    return (
        <div>
            <h2>Gift !</h2>
            <button className="btn-add" onClick={addGifts}>Add gift</button>
        </div>
    );
}