import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { TextField, Grid, Paper, Container, AppBar, Toolbar, IconButton } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import {
    makeStyles,
    ThemeProvider,
    createMuiTheme,
} from '@material-ui/core/styles';
import { green, orange } from '@material-ui/core/colors';
import 'fontsource-roboto';


const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #abc, #678)',
        border: 0,
        borderRadius: 10,
        color: 'green',
        padding: '0 30px',
        marginBottom: '15px'
    },
});

const newTheme = createMuiTheme({
    typography: {
        h2: {
            fontSize: 50,
        },
        body1: {
            fontSize: 25,
        },
    },
    palette: {
        primary: {
            main: green[300],
        },
        secondary: {
            main: orange[600],
        },
    },
});

function CheckBoxSample() {
    const [checked, setChecked] = useState(true);

    return (
        <>
            <FormControlLabel
                control={
                    <Checkbox
                        checked={checked}
                        icon={<SaveIcon />}
                        checkedIcon={<SaveIcon />}
                        onChange={(e) => setChecked(e.target.checked)}
                        color="secondary"
                    />
                }
                label="testing checkbox"
            />
            <h3>{checked.toString()}</h3>
        </>
    );
}

function InputSample() {
    return (
        <>
            <TextField
                id=""
                type="email"
                label="the email"
                placeholder="example@mail.com"
            />
        </>
    );
}

function StyledButton() {
    const classes = useStyles();

    return <Button className={classes.root}>Styled Button</Button>;
}

function Bar() {
    return (
        <AppBar position="static" color="secondary">
            <Toolbar>
                <IconButton edge="start" >
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" style={{ flexGrow: 1}}>
              Material UIApp
                </Typography>
                <Button>Login</Button>
            </Toolbar>
        </AppBar>
    );
}
export default function MaterialComponents() {
    return (
        <ThemeProvider theme={newTheme}>
            <Container >
                <Bar/>
                <Typography variant="h2">Welcome to machine</Typography>
                <Typography variant="body1" color="initial">
                    Listen pink floyd
                </Typography>
                <StyledButton />
                <Grid
                    container
                    spacing={2}
                    justify="center"
                    style={{ backgroundColor: 'gray' }}
                >
                    <Grid item xs={3} sm={6}>
                        <Paper style={{ height: 75, width: '100%' }} />
                    </Grid>
                    <Grid item xs={3} sm={3}>
                        <Paper style={{ height: 75, width: '100%' }} />
                    </Grid>
                    <Grid item xs={3} sm={3}>
                        <Paper style={{ height: 75, width: '100%' }} />
                    </Grid>
                </Grid>

                <InputSample />
                <br />
                <CheckBoxSample />
                <Button
                    variant="contained"
                    color="primary"
                    startIcon={<SaveIcon />}
                >
                    save
                </Button>
            </Container>
        </ThemeProvider>
    );
}