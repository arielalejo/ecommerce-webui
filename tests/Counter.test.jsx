import React from 'react';
import {shallow, mount} from 'enzyme';
import Counter from '../src/material-ui-tuto/Counter';

it('should render App Component', ()=>{
    const app = shallow(<Counter/>);
    
    expect(app).toMatchSnapshot();
    expect(app.find('h2').length).toEqual(1);
});

it('should change the `gifts` state when the `Add gift` button is pressed', ()=>{
    const setGifts = jest.fn();
    const app = mount(<Counter onClick={setGifts}/>);
    // jest.spyOn(React, "useState").mockImplementation(gifts => [gifts, setGifts]);


    app.find('.btn-add').simulate('click');

    // expect(setGifts).toBeTruthy();       
    // expect(setGifts).toHaveBeenCalled();
});