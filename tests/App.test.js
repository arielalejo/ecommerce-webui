import {shallow } from 'enzyme';
import App from '../src/App';
import React from 'react';


it('should render App Component', ()=>{
    const app = shallow(<App/>);
    
    expect(app).toMatchSnapshot();
});


